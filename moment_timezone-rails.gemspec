# -*- encoding: utf-8 -*-
# stub: moment_timezone-rails 0.4.0 ruby lib

Gem::Specification.new do |s|
  s.name = "moment_timezone-rails"
  s.version = "0.4.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Lim Victor"]
  s.date = "2016-07-28"
  s.description = "moment-timezone for Rails"
  s.email = ["github.victor@gmail.com"]
  s.files = [".gitignore", "Gemfile", "LICENSE", "LICENSE.txt", "README.md", "Rakefile", "lib/moment_timezone/rails.rb", "lib/moment_timezone/rails/version.rb", "moment_timezone-rails.gemspec", "vendor/assets/javascripts/moment-timezone-with-data-2010-2020.js", "vendor/assets/javascripts/moment-timezone-with-data-2010-2020.min.js", "vendor/assets/javascripts/moment-timezone-with-data.js", "vendor/assets/javascripts/moment-timezone-with-data.min.js", "vendor/assets/javascripts/moment-timezone.js", "vendor/assets/javascripts/moment-timezone.min.js"]
  s.homepage = "https://github.com/viclim/moment_timezone-rails"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "moment-timezone-0.4.0"

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rails>, ["~> 3.2"])
      s.add_runtime_dependency(%q<momentjs-rails>, ["~> 2.10"])
    else
      s.add_dependency(%q<rails>, ["~> 3.2"])
      s.add_dependency(%q<momentjs-rails>, ["~> 2.10"])
    end
  else
    s.add_dependency(%q<rails>, ["~> 3.2"])
    s.add_dependency(%q<momentjs-rails>, ["~> 2.10"])
  end
end
